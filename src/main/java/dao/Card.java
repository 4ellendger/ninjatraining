package dao;

public class Card {
    public final int id;
    public final String name;
    public final boolean isFree;
    public final String pictureUrl;
    public final Record[] records;

    public Card(int id, String name, boolean isFree, String pictureUrl, Record[] records) {
        this.id = id;
        this.name = name;
        this.isFree = isFree;
        this.pictureUrl = pictureUrl;
        this.records = records;
    }
}
