package dao;

import java.util.Date;

public class Record {
    public final int id;
    public final String name;
    public final Date start;
    public final Date duration;
    
    public Record(int id, String name, Date start, Date duration) {
        this.id = id;
        this.name = name;
        this.start = start;
        this.duration = duration;
    }
}
